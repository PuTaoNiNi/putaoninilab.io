---
title: '[.NET] 如何透過 DisplayName 或 Description 自訂 Enum 字串'
categories: .NET
keywords: >-
  .NET, C#, DisplayName, Description, GetMember, GetField, Enum, 枚舉, 字串,
  Extension Methods, 擴充方法
summary: enum 由一組整數類型命名的常數定義，其為實值類型。如何透過 DisplayName 或 Description 自訂 Enum 字串。
tags:
  - .NET
  - Enum
abbrlink: 3485751689
date: 2022-03-30 15:01:29
---

## [.NET] 如何透過 DisplayName 或 Description 自訂 Enum 字串

>An enumeration type (or enum type) is a value type defined by a set of named constants of the underlying integral numeric type

1. enum 由一組整數類型命名的常數定義，其為實值類型。

2. 類型可為 sbyte、byte、short、ushort、int、uint、long、ulong。

```csharp
public enum Wood : short
{
    Mahogany,
    Maple,
    Cocobolo,
    Cedar,
    Adirondack,
    Alder,
    Sitka,
}
```

常被用來當作"狀態"的判定，如:

```csharp
if(httpStatus == HttpStatusCode.OK) {
    // do something
}
```

也可能會被哪來當作"下拉選單"的條件，如果單純用enum原本名稱顯示遠遠不夠，故下面示範如何自訂與取得想要的字串

```csharp
// 擴增函式可以透過 "." 方式使用
Wood.Adirondack.GetDisplayName();
Wood.Adirondack.GetDescription();
```

## 用 GetMember

```csharp
public static string GetDisplayName(this Enum enumValue)
{
    var name = enumValue.ToString();
    var memberInfos = enumValue.GetType().GetMember(name);
    var displayName = name;

    if (memberInfos.Any())
    {
        var attribute = memberInfos.First().GetCustomAttribute<DisplayAttribute>();
        displayName = attribute?.GetName() ?? name;
    }

    return displayName;
}
```

## 用 GetField (建議)

```csharp
public static string GetDescription(this Enum enumValue)
{
    var name = enumValue.ToString();
    var field = enumValue.GetType().GetField(name);
    var description = name;

    if (field != null)
    {
        var attribute = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
        description = attribute?.Description ?? name;
    }

    return description;
}
```

## 參考

[Enum ToString with user friendly strings](https://stackoverflow.com/questions/479410/enum-tostring-with-user-friendly-strings)

[Can my enums have friendly names?](https://stackoverflow.com/questions/1415140/can-my-enums-have-friendly-names)

[Difference between Display and Description attribute](https://stackoverflow.com/questions/23082249/difference-between-display-and-description-attribute)

[GetMember vs GetField performance in C#](https://stackoverflow.com/questions/23082249/difference-between-display-and-description-attribute)

[microsoft - Enum](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/enum)

[microsoft - Extension Methods](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/extension-methods#example)

[microsoft - Integral numeric types](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/integral-numeric-types)
