---
title: "[工具] 網路工具-SharpLab"
categories: Tool
keywords: ".NET, C#, F#, VB, IL, 反編譯, 語法糖"
summary: "透過 SharpLab 可以瞭解 C#語法糖（Syntactic sugar）與 IL（Intermediate Language），當然也可以透過一些工具去反編譯出來，但身為工程師就是懶，能少做一些事情就少做。"
tags:
  - Tool
  - .NET
abbrlink: 3902068567
date: 2021-10-30 12:46:42
---

## [SharpLab](https://sharplab.io/)

### 用途

透過 **SharpLab** 可以瞭解 C#語法糖（Syntactic sugar）與 IL（Intermediate Language），當然也可以透過一些工具去反編譯出來，但身為工程師就是~~懶~~，能少做一些事情就少做!

### 如何使用

![操作介面](Operation.webp)

1. 選擇程式語言 (C#, VB, F#)
2. 輸出的結果
3. Build 結果 (Debug, Release)

### 語法糖

> 語法糖（Syntactic sugar）: 是由英國"彼得·蘭丁"發明的一個術語，方便程式設計師使用，此語法對語言的功能並**沒有影響**。

例如下面寫法用語法糖寫出來最後跟一般寫法相同:

```csharp
// 語法糖
public string MyName { get; set; }
```

用了語法糖的程式碼是不是簡單許多，不用在寫一長串程式碼

```csharp
// 一般寫GET/SET寫法
private string _myName;
public string MyName
{
    get { return _myName; }
    set { _myName = value; }
}
```

### 中間語言

> IL（Intermediate Language）: 中文就是"中間語言"。CIL 類似一個物件導向的組合語言，並且它是完全基於**堆疊**。

我們用常見 string fomat 來看編譯出來是什麼

```csharp
var concat = 100;
_ = $"Hello {concat}";
```

可以看到轉換成 IL 後程式實際做了哪些事情，如果有細心觀察一下會發現裡面偷偷做了**boxing**的動作。

(~~:flags:~~ 之後會補充說明**boxing**跟**unboxing**) [補充][boxing-and-unboxing]

```IL
IL_0000: nop
IL_0001: ldc.i4.s 100
IL_0003: stloc.0
IL_0004: ldstr "Hello {0}"
IL_0009: ldloc.0
IL_000a: box [System.Private.CoreLib]System.Int32
IL_000f: call string [System.Private.CoreLib]System.String::Format(string, object)
IL_0014: pop
IL_0015: ret
```

### 參考資料

- [語法糖](https://zh.wikipedia.org/wiki/%E8%AF%AD%E6%B3%95%E7%B3%96)
- [通用中間語言](https://zh.wikipedia.org/wiki/%E9%80%9A%E7%94%A8%E4%B8%AD%E9%97%B4%E8%AF%AD%E8%A8%80)

[boxing-and-unboxing]: https://putaonini.gitlab.io/net/20211118/3529614637/
