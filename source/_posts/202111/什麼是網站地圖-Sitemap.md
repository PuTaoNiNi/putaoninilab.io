---
title: "[SEO] 什麼是網站地圖 Sitemap"
categories: SEO
keywords: "SEO, 網站地圖, Sitemap"
summary: "介紹網站地圖 Sitemap 概念與設定上有需要注意的細節，它是一種用來提供網站資訊的檔案，您可以在其中列出網頁、影片和其他檔案的資訊，並呈現這些內容彼此間的關係。"
tags:
  - SEO
abbrlink: 1832507634
date: 2021-11-02 20:08:30
---

## [SEO] 什麼是網站地圖 Sitemap

介紹網站地圖 Sitemap 概念與設定上有需要注意的細節。

### Sitemap 是什麼

一種用來提供網站資訊的檔案，您可以在其中列出網頁、影片和其他檔案的資訊，並呈現這些內容彼此間的關係。Google 等**搜尋引擎**都會讀取網站的 Sitemap 檔案，藉此以更有效率的方式檢索網站。

簡單來說就是**網站地圖**，透過 Sitemap 爬蟲可以快速知道網站內有哪些資訊，可以更有效地檢索網站。

### Sitemap 格式

以 Google 為例所支援格式:

- XML
- RSS、mRSS 和 Atom 1.0
- 文字

> **_注意_**</span>:
>
> - 單一 Sitemap 在未壓縮時的上限為 50MB，且最多只能 50,000 個網址。(網址超過時需分割數個較小的 Sitemap)

#### XML

格式須符合[Sitemap 通訊協定][sitemaps]，下面只針對基本介紹，如果你的網站較為複雜如多語系(網址不同)或分電腦跟手機版，可以參考協定去設定

> **_注意_**</span>:
>
> - 所有的值都必須 **實體逸出(entity-escaped)**
> - Google 目前不支援在 Sitemap 中的 `<priority>` 屬性
> - 不要使用相對網址，例如 `./mypage.html`

```XML
<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
      <loc>http://www.example.com/</loc>
      <lastmod>2021-01-01</lastmod>
      <changefreq>monthly</changefreq>
      <priority>0.8</priority>
    </url>
    <url>
        <loc>http://www.example.com/result</loc>
        <lastmod>2021-11-11</lastmod>
    </url>
   ...
</urlset>
```

##### Sitemap 標記定義

|     屬性      | 必要 | 說明                                                                                                                                                                                            |
| :-----------: | :--: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|    \<loc>     |  必  | 網頁的 URL<br> <ul><li>通訊協定開頭(例如 http)</li><li>需少於 2,048 個字元</li></ul>                                                                                                            |
|  \<lastmod>   |  必  | 最後修改日期<br> <ul><li>採用 [W3C 日期格式][w3c]</li><li>如果與 Header 裡面的[If-Modified-Since (304)][if-modified-since]不同，搜尋引擎可能會用不同方式處理</li></ul>                          |
| \<changefreq> |      | 網頁更新的頻率，此值只做為提示而非指令，主要還是看搜尋引擎怎麼使用此值去做處理<ul><li>always</li><li>hourly</li><li>daily</li><li>weekly</li><li>monthly</li><li>yearly</li><li>never</li></ul> |
|  \<priority>  |      | URL 的優先順序。<ul><li>有效值的範圍為 0.0 到 1.0</li><li>設定此值，並不會影響在**搜尋引擎網站排名**</li></ul>                                                                                  |

##### Entity-escaped

|  字元  |     | 逸出碼  |
| :----: | :-: | :-----: |
| & 符號 |  &  | \&amp;  |
| 單引號 |  '  | \&apos; |
| 雙引號 |  "  | \&quot; |
|  大於  |  >  |  \&gt;  |
|  小於  |  <  |  \&lt;  |

```xml
問題:
http://www.example.com/view?widget=3&count>2

正確:
http://www.example.com/view?widget=3&amp;count&gt;2
```

#### RSS、mRSS 和 Atom 1.0

網誌含有 RSS 或 Atom 動態消息，您可以透過 Sitemap 提交動態消息的網址。

> **_注意_**:
>
> - 支援 RSS 2.0 和 Atom 1.0 動態消息。
> - 可以使用 mRSS (媒體 RSS) 動態消息將網站中影片內容。

#### 文字

將每個網址分行列出，例如：

```txt
http://www.example.com/
http://www.example.com/result
```

> **_注意_**</span>:
>
> - 檔案需使用 **UTF-8**。
> - 請勿加入網址以外的內容。
> - 檔名沒有命名規定，但副檔名必須是 **.txt** ，例如 sitemap.txt。

### 參考資料

- [Sitemaps][sitemaps]
- [Google Sitmap][google-sitmap]
- [If-Modified-Since][if-modified-since]

[sitemaps]: https://www.sitemaps.org/protocol.html
[google-sitmap]: https://developers.google.com/search/docs/advanced/sitemaps/build-sitemap?hl=zh-tw
[w3c]: https://www.w3.org/TR/NOTE-datetime
[if-modified-since]: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/If-Modified-Since
