---
title: "[工具] Chocolatey 常用指令"
categories: Tool
keywords: "Chocolatey, Chocolatey 常用指令, Chocolatey GUI"
summary: "介紹 Chocolatey 常用指令，透過指令可以安裝、升級、移除套件。防止之後自己安裝套件，還需要去查詢指令。"
tags:
  - Tool
  - Chocolatey
abbrlink: 2917994173
date: 2021-11-09 17:47:53
---

## [工具] Chocolatey 常用指令

介紹 Chocolatey 常用指令，透過指令可以安裝、升級、移除套件。防止之後自己安裝套件，還需要去查詢指令。

### 全部指令 ([參考][commands])

下面列出目前 `Chocolatey` 所有指令，但指令不一定可以使用，會依據版本的不同功能會有所差異 ~~，想當個免費仔也太難~~ ，差異的部分可以參考[這裡][compare]，所以我只會針對常用的部分作詳細介紹。

#### 一般

- search - 搜尋遠端或本地套件
- find - 搜尋遠端或本地套件
- list - 列出遠端或本地套件
- info - 查詢套件資訊
- install - 從設置來源(sources)安裝套件
- uninstall - 解除安裝套件
- outdated - 列出過時的套件
- upgrade - 從設置來源(sources)更新套件
- pin - 標示禁止升級套件
- export - 導出當前安裝的套件列表
- help - 顯示 `choco` 的幫助信息

#### 設置

- config - 檢索和設置設定文件設置
- source - 查看和設置預設來源
- sources - 查看和設置預設來源 （來源的別名）
- feature - 查看和設置 choco 功能
- features - 查看和設置 choco 功能 （功能的別名）
- apikey - 查看、保存或刪除特定源(sources)的 `apikey`
- setapikey - 查看、保存或刪除特定源(sources)的 `apikey`（apikey 的別名）
- unpackself - 重新安裝 `Chocolatey` 基礎文件

#### 套件

- new - 從模板生成`Chocolatey`包所需的檔案
- pack - 將 `nuspec`、腳本和其他 `Chocolatey` 包資源打包到一個 `nupkg`
- push - 將編譯的 `nupkg` 推送到來源

#### 其他

- support - 提供支援訊息 ([參考][sync])
- synchronize - 與系統安裝的套件同步 - 下載丟失的套件 ([參考][support])
- sync - 與系統安裝的套件同步 - 下載丟失的套件 ([參考][sync])
- download - 下載套件
- optimize - 優化安裝，減少空間使用

### 常用指令

#### search/find

```CMD
:: 查詢套件
choco search git

:: 查詢套件 (本地)
choco search git -lo

:: 查詢套件 (有驗證)
choco search git -a

:: 查詢套件 (精準)
choco search git -e
```

#### list

```CMD
:: 列出套件
choco list

:: 列出套件 (本地)
choco list -lo

:: 列出套件 (有驗證)
choco list -a

:: 列出套件 (分頁)
choco list --page=0 --page-size=25
```

#### info

```CMD
:: 套件資訊
choco info git
```

#### install

```CMD
:: 安裝套件
choco install git

:: 安裝套件 (略過提示)
choco install git -y
```

#### uninstall

```CMD
:: 解除安裝套件
choco uninstall git

:: 解除安裝套件 (略過提示)
choco uninstall git -y
```

#### outdated

```CMD
:: 查詢過時套件
choco outdated
```

結果

```CMD
:: package name | current version | available version | pinned

chocolatey-visualstudio.extension|1.9.0|1.10.0|false
Everything|1.4.11005|1.4.11009|false
winrar|6.01|6.02|true
```

#### upgrade

```CMD
:: 更新套件
choco upgrade git

:: 更新套件 (略過提示)
choco upgrade git -y

:: 更新套件 (全部)
choco upgrade all
```

#### pin (標示不升級)

```CMD
:: 列出已被標示
choco pin

:: 標示套件為不升級
choco pin add -n git

:: 解除標示
choco pin remove -n git
```

### Chocolatey GUI

看上面滿滿的指令，會不會大家跟我一樣都沒有辦法記起來，如果有好用工具可以讓我們可以不用在記這些繁瑣的指令該有多好，還真的有一套工具叫 `Chocolatey GUI`，之後會來好好介紹工具如何使用。

[解決記 choco 指令的煩惱 - Chocolatey GUI](https://putaonini.gitlab.io/tool/20211110/3900099193)

### 參考資料

[choco 指令][commands]

[版本比較][compare]

[commands]: https://docs.chocolatey.org/en-us/choco/commands/
[compare]: https://chocolatey.org/compare
[support]: https://chocolatey.org/support
[sync]: https://docs.chocolatey.org/en-us/features/package-synchronization
