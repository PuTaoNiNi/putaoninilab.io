---
title: "[SEO] 如何在 Hexo 建立網站地圖 Sitemap"
categories: SEO
keywords: "Hexo, SEO, 網站地圖, Sitemap"
summary: "介紹如何快速建立網站地圖(Sitemap.xml)，如果每次新增網頁都要修改也太麻煩，所以有大神已經幫我們寫好套件 hexo-generator-sitemap，只要安裝套件後便可以自動產生 Sitemap.xml。"
tags:
  - SEO
abbrlink: 3300325365
date: 2021-11-04 11:11:22
---

## [SEO] 如何在 Hexo 建立網站地圖 Sitemap

之前有介紹[Sitemap][sitemap]是什麼，如果每次新增網頁都要修改 Sitemap.xml 也太麻煩，所以有大神已經幫我們寫好套件 `hexo-generator-sitemap`，只要安裝套件後便可以自動產生 Sitemap.xml。

### 安裝套件

```Bash
npm install hexo-generator-sitemap
```

### 設定套件

可以在`_config.yml`設定檔新增套件設定，如果不設定也可以產生。因為套件本身有[預設值][config-hexo-generator-sitemap]。

**注意:** 並非樣板(themes)目錄底下的`_config.yml`設定檔。

```Yaml
# hexo-generator-sitemap
sitemap:
  path: sitemap.xml
  # template: ./sitemap_template.xml #有自訂樣板需求在設定
  rel: false
  tags: true
  categories: true
```

- path : Sitemap 路徑與名稱 (Default: sitemap.xml)
- template : 自訂 Sitemap 樣板 ([default template][git-hexo-generator-sitemap])
- rel : 將網站的 header 新增[`rel-sitemap`][rel-sitemap] (Default: false)
- tags : 增加 tags 資料到 sitemap.xml
- categories : 增加 categories 資料到 sitemap.xml

> - **注意:** `rel` 這個設定，如果網站的 header 格式下面這樣，將會失效
>
> ```htmlembedded
> <head>
>    <title>...</title>
> </head>
> ```
>
> 必須格式
>
> ```htmlembedded
> <head><title>...</title></head>
> ```
>
> 目前已經有跟作者提 [Issue](https://github.com/hexojs/hexo-generator-sitemap/issues/129)

重新編譯一次，並發布

```BASH
hexo g -d
```

檢查一下 sitemap.xml 是否有正常產生

1. 檢查目錄 `\public\sitemap.xml`
2. 自己網站 `{自己網站}\sitemap.xml`

### 排除設定

設定 `sitemap: false`到你的**HEXO**文章或網站，來進行排除

```HEXO
---
title: lorem ipsum
date: 2020-01-02
sitemap: false
---
```

### 參考資料

[hexo-generator-sitemap][git-hexo-generator-sitemap]
[rel-sitemap][rel-sitemap]

[git-hexo-generator-sitemap]: https://github.com/hexojs/hexo-generator-sitemap
[sitemap-template]: https://github.com/hexojs/hexo-generator-sitemap/blob/master/sitemap.xml
[config-hexo-generator-sitemap]: https://github.com/hexojs/hexo-generator-sitemap/blob/41bcffe92caec208baabcc192647becb84da70d1/test/index.js#L8
[search-console]: https://search.google.com/search-console/about
[rel-sitemap]: http://microformats.org/wiki/rel-sitemap
[sitemap]: https://putaonini.gitlab.io/seo/20211102/1832507634/
