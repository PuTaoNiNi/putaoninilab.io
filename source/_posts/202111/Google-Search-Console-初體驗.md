---
title: "[SEO] Google Search Console 初體驗"
categories: SEO
keywords: "SEO, Google Search Console"
summary: "介紹 Google Search Console 如何設定與初步使用，可以透過這項服務去改善索引上的問題，並協助你監控與維持網站在 Google 搜尋結果中的排名。"
tags:
  - SEO
abbrlink: 2384807159
date: 2021-11-08 18:52:35
---

## [SEO] Google Search Console 初體驗

### Google Search Console 是什麼?

Google Search Console 是一項免費服務，能夠協助你監控與維持網站在 Google 搜尋結果中的排名，並可以透過這項服務去改善或是排除索引上的問題。

### 開始使用 Google Search Console

#### 選取資料類型

一開始必須選擇資料類型，分成`網域`跟`網址`，文章會以網址範例來介紹:

![資料類型](DataType.webp)

填入自己網站網址後，會需要驗證這個網站是否是你的，目前的驗證方式有

![驗證方式](VerifyMethod.webp)

##### HTML 檔案 (建議)

這個方式超級簡單就把 Google 提供檔案(`googleXXX.html`)下載下來，上傳你的網站裡面就結束。

##### HTML 標記

這個方式需要將特定文字，複製進`<head>`裡面。

```HTML
<head>
    <meta name="google-site-verification" content="XXX" />
</head>
```

##### Google Analytics(分析)

如果你的網站有使用 `Google Analytics` 可以直接點驗證，但你必須擁有 `Google Analytics` **「編輯」** 權限才行。

##### ~~Google 代碼管理工具 (暫時不介紹)~~

##### ~~網域名稱供應商 (暫時不介紹)~~

### 提交 sitemap.xml

如果不知道如何產生 `sitemap.xml` 可以參考[此篇]][sitemap]自行建立或是使用[XML sitemap Generator][xml sitemap generator]建立

> 注意: XML sitemap Generator 建立上限為 500 個網址

"索引 > Sitemap"裡面的來新增 `sitemap.xml`，新增後可以看一下狀態是否為**成功**

![Sitemap](Sitemap.webp)

如果狀態不是**成功**，可以點 `開啟SITEMAP` 來檢查`sitemap.xml`是否可以正常開啟並確認內容，如果都正確無誤就等個 1~2 天檢查是否狀態有改變，~~軟爛的葡萄泥也是苦命等待 2 天狀態才被改變~~。

![Sitemap status](SitemapStatus.webp)

### 設定 robots.txt

```SEO
User-agent: Googlebot
Disallow: /nogooglebot/

User-agent: *
Allow: /

Sitemap: http://www.example.com/sitemap.xml
```

| 屬性       | 說明                                                                               |
| :--------- | :--------------------------------------------------------------------------------- |
| User-agent | 可以針對特定檢索器([參考][crawlers])進行設定，如果用`*`表示全部(AdsBot 檢索器以外) |
| Disallow   | 禁止檢索器檢索網站的目錄或網頁                                                     |
| Allow      | 允許檢索器檢索網站的目錄或網頁                                                     |
| sitemap    | 該網站的 Sitemap 所在位置                                                          |

> 注意: 每項規則至少要有一個 `disallow` 或 `allow` 項目

上傳完 `robots.txt` 可以開啟 "[robots.txt 測試工具][robots test]" 來進行測試，來驗證是否正確設定。

![robots tool](robots.webp)

### 參考資料

[Google Search Console][google search console]
[robots.txt 指令][robots rules]
[Google 檢索器總覽][crawlers]
[XML sitemap Generator][xml sitemap generator]

[google search console]: https://search.google.com/search-console/about
[robots]: https://developers.google.com/search/docs/advanced/robots/create-robots-txt?hl=zh-tw
[robots rules]: https://developers.google.com/search/docs/advanced/robots/create-robots-txt?hl=zh-tw#create_rules
[robots test]: https://support.google.com/webmasters/answer/6062598?hl=zh-Hant
[crawlers]: https://developers.google.com/search/docs/advanced/crawling/overview-google-crawlers?hl=zh-tw
[sitemap]: https://putaonini.gitlab.io/seo/20211102/1832507634/
[xml sitemap generator]: https://www.xml-sitemaps.com/
