---
title: "[工具] 解決記 choco 指令的煩惱 - Chocolatey GUI"
categories: Tool
keywords: "Chocolatey, Chocolatey GUI, choco"
summary: "Chocolatey GUI 是一個簡單的應用程式，可以讓你在 Windows 上安裝和管理 Chocolatey 所提供的套件。"
tags:
  - Tool
  - Chocolatey
abbrlink: 3900099193
date: 2021-11-10 14:53:58
---

## [工具] 解決記 choco 指令的煩惱 - Chocolatey GUI

Chocolatey GUI 是一個簡單的應用程式，可以讓你在 Windows 上安裝和管理 Chocolatey 所提供的套件。

### 如何安裝 Chocolatey GUI

只要執行下面指令就可以安裝 `Chocolatey GUI`

```CMD
choco install chocolateygui
```

### 操作 Chocolatey GUI

安裝完後執行的畫面，可以透過選擇`This PC` (本機) 跟 `Chocolatey` 進行切換，查看本機安裝那些套件跟 Chocolatey 有哪些套件可以安裝。

![Chocolatey GUI](ChocolateyGUI.webp)

如果套件已經**過時**會被特別表示 **`Out of data`**，可以依據需求進行更新

![Out of data](OutOfData.webp)

可以透過點擊套件進入詳細資料進行操作，可以表示不升級、重新安裝等等操作

![Package Details](PackageDetails.webp)

如果表示表示不升級介面上會顯示小圖釘表示

![Pin](Pin.webp)

### 設定 Chocolatey GUI

可以針對 `Chocolatey GUI` 跟 `Chocolatey` 進行相關設定

![Chocolatey Settings](ChocolateySettings.webp)

### 結論

透過 `Chocolatey GUI` 就可以不用記繁雜的指令可以透過 GUI 介面進行操作，也可以透過介面知道自己已經安裝那些套件，管理上也比打指令來得方便，如果對學習指令有興趣也可以參考([此篇][chocolatey 常用指令])來學習。

### 參考資料

[chocolatey gui](https://docs.chocolatey.org/en-us/chocolatey-gui/)

[chocolatey commands](https://docs.chocolatey.org/en-us/choco/commands/)

[chocolatey 常用指令]: https://putaonini.gitlab.io/tool/20211109/2917994173/
