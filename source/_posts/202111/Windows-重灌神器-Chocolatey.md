---
title: "[工具] Windows 重灌神器 - Chocolatey"
categories: Tool
keywords: "Chocolatey, Chocolatey 安裝, 重灌"
summary: "Chocolatey 是什麼呢? 可以吃嗎?，Chocolatey 它是一套專門管理 Windows 套件管理器，有利於電腦重灌與管理套件。"
tags:
  - Tool
  - Chocolatey
abbrlink: 1382257806
date: 2021-11-08 16:21:05
---

## [工具] Windows 重灌神器 - Chocolatey

介紹 Chocolatey 它是一套專門管理 Windows 套件管理器，有利於電腦重灌與管理套件。

### Chocolatey 是什麼呢? 為什麼是神器?

Chocolatey 是什麼呢? ~~可以吃嗎?~~，Chocolatey 是一套專門管理 Windows 套件管理器，類似 Linux 的 RPM 與 MAC 的 Homebrew，所以你可以建立自己套件包來快速部署任何 Windows 系統上，在自動部屬的期間你可以喝杯咖啡或是呼吸新鮮空氣，是不是很好呢? 哪我們下面就介紹如何使用 Chocolatey!

### 安裝 Chocolatey

安裝方式有兩種 **(安裝時都需要用系統管理身分執行)**

#### cmd

用命令提示字元執行以下指令

```CMD
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```

#### Powershell ([參考][install-chocolatey])

使用 Powershell 安裝時要先注意執行原則是否有被限制，可以執行下指令取得

```Powershell
Get-ExecutionPolicy
```

如果顯示的是 `Restricted` 則需變更 PowerShell 執行原則設定，可以執行下指令(擇一就可)，變更執行原則時會提示是否變更選擇 **`[Y]`** 就可

```Powershell
Set-ExecutionPolicy AllSigned
```

or

```Powershell
Set-ExecutionPolicy Bypass -Scope Process
```

執行下列指令就開始安裝 chocolatey

```Powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

如果安裝成功後，輸入 `choco` 指令，就會跳出版本如下

```CMD
C:\>choco
Chocolatey v0.11.3
Please run 'choco -?' or 'choco <command> -?' for help menu.
```

> 注意: Chocolatey 是可以改安裝路徑(不建議) [官方文件][different-location]
>
> - 未安裝
>   1. 新加一個環境變數`ChocolateyInstall`並設定成想要路徑例如 `D:\Chocolatey` >
>      ![ChocolateyInstall](ChocolateyInstall.webp)
>   2. 手動建立資料夾
>   3. 依據上面步驟安裝 Chocolatey
> - 已安裝
>   1. 將 `ChocolateyInstall` 改成想要路徑
>   2. 重新安裝 Chocolatey
>   3. 將原本 `C:\ProgramData\chocolatey` 路徑裡的 lib/bin 複製到新路徑底下
>   4. 將原本刪除目錄刪除

### 如何安裝 Package ([傳送門][packages])

可以透過官網上找自己想要的套件
![packages](Packages.webp)

> 注意: 如果要安裝**綠色軟體**，可以找看看要安裝的套件是否有 `Portable`

執行下面指令就開始安裝套件(用 WinRAR 範例)

```CMD
choco install winrar
```

你會發現如果執行上面指令，還是會提示你是否要繼續進行腳本，這時候只要將指令後面加上 **`-y`**，這時候神奇事情就發生，你就不需要操作什麼套件自動幫你安裝好，佛系安裝:緣分到了，套件自動安裝好(誤)。

```CMD
choco install winrar -y
```

### 解除安裝 Package

解除安裝也很簡單，只要執行下面指令 (如果想要自動跑也是指令後面加上 **`-y`**)

```CMD
choco uninstall winrar
```

### 參考資料

[chocolatey.org](https://community.chocolatey.org/)
[Chocolatey 維基](https://zh.wikipedia.org/wiki/Chocolatey)

[install-chocolatey]: https://chocolatey.org/install
[packages]: https://community.chocolatey.org/packages
[different-location]: https://docs.chocolatey.org/en-us/choco/setup#installing-to-a-different-location
