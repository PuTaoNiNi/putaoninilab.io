---
title: '[.NET] Blazor WebAssembly 部署到 GitHub Pages'
categories: .NET
keywords: '.NET, C#, Blazor WebAssembly, Blazor, GitHub Pages, 部署, Deploy'
summary: 此篇為 Blazor WebAssembly 部屬到 GitHub Pages 上的操作流程，並記錄部屬上所遇到問題與解決方法。
tags:
    - .NET
    - Blazor
abbrlink: 2014914482
date: 2022-05-16 14:56:17
---

## [.NET] 將 Blazor WebAssembly 部署到 GitHub Pages

隨著前端技術日新月異，微軟自己也推出了前端框架(Blazor)，如果 Blazor WebAssembly 是純前端網站，我就在思考是否可以將它，部屬到 GitHub Pages 上執行呢? 透過找尋幾位大神的部落格，發現真的可以將 Blazor WebAssembly 部屬到 GitHub Pages 上，只是需要注意一些小細節，此篇就是我記錄操作流程。

## 建立 Blazor WebAssembly App 專案

1. 執行下列的指令可以建立，Blazor WebAssembly App 專案，並使用 VS Code 開啟專案看一下檔案。

    ```bash
    # 建立 BlazorGitHubPages 目錄
    mkdir BlazorGitHubPages
    cd BlazorGitHubPages

    # 建立 Blazor WebAssembly App 專案
    dotnet new blazorwasm

    # 使用 VSCode 開啟專案
    code .
    ```

    ![Blazor WebAssembly App 專案](ZO15pkS.webp)

2. 使用 `dotnet run` 測試一下，專案是否能正常執行

    ```bash
    dotnet run

    # 正在建置...
    # info: Microsoft.Hosting.Lifetime[14]
    #       Now listening on: https://localhost:7019
    # info: Microsoft.Hosting.Lifetime[14]
    #       Now listening on: http://localhost:5089
    # info: Microsoft.Hosting.Lifetime[0]
    #       Application started. Press Ctrl+C to shut down.
    # info: Microsoft.Hosting.Lifetime[0]
    #       Hosting environment: Development
    # info: Microsoft.Hosting.Lifetime[0]
    #       Content root path: C:\Users\Grape\BlazorGitHubPages
    # info: Microsoft.Hosting.Lifetime[0]
    #       Application is shutting down...
    ```

## Push 專案到 GitHub

1. 透過下列指令可以建立 `gitignore`，初始化 git repositry，並提交版本。

    ```bash
    # 建立 gitignore
    dotnet new gitignore

    # 建立 git repositry
    git init

    # 將所有差異加入索引
    git add --all

    # 提交版本
    git commit -m "init"
    ```

2. 將檔案 Push to GitHub reomte。

    ```bash
    # 新增 remote (需要改成自己 remote)
    git remote add origin https://github.com/[USERNAME]/[REPOSITORY_NAME].git

    # Push to GitHub
    git push -u origin master
    ```

    如果按照上面指令執行後，便可以在 GitHub 上看見檔案被推上去。

    ![Push to GitHub](xgvZM1U.webp)

## 建立 GitHub Actions

1. 按 GitHub repositry 上 Actions 按鈕，透過搜尋 `.net` 來找 .NET 範本。

    ![新增 GitHub Action](8kvUKQt.webp)

2. 選擇 .NET 範本來進行新增。

    ![原始的yml檔](nB5Ru5B.webp)

3. 將 YML 檔修改成下面。

    ```YML
    name: .NET

    on:
    push:
        branches: [ master ]

    jobs:
    deploy-to-github-pages:

        runs-on: ubuntu-latest

        steps:
        - uses: actions/checkout@v3
        - name: Setup .NET
        uses: actions/setup-dotnet@v2
        with:
            dotnet-version: 6.0.x
        
        # 修改成專案名稱    
        - name: Publish .NET Core Project
        run: dotnet publish [REPOSITORY_NAME].csproj -c Release -o release --nologo
        
        - name: Commit wwwroot to GitHub Pages
        uses: JamesIves/github-pages-deploy-action@v4.3.3
        with:
            BRANCH: gh-pages
            FOLDER: release/wwwroot
    ```

### Actions 執行時可能遇到問題

```bash
fatal: unable to access 'xxx': The requested URL returned error: 403
```

![Error 403](NOVlitX.webp)

如果在執行 Actions 執行時，可能會遇到 403 問題，哪就表示 workflow 權限不足，需要開啟權限，開啟方式如下:

1. 按 GitHub repositry 上 General 按鈕，並點擊 Actions 內的 General。

    ![Actions permissions](DhSouGG.webp)

2. 找尋 Workflow permissions，並將權限改成 "Read and write permissions"。

    ![Read and write permissions](wELb5Dy.webp)

3. 設定完後並重新執行失敗的 Action，就可以成功被執行。

    ![Re-run failed jobs](Qmy7m8e.webp)

## 設定 GitHub Pages

按 GitHub repositry 上 General 按鈕，點擊 Pages 並將 Source 設定改成 gh-pages branch，之後就可以點網址，看是否有成功將網站部屬上去。~~(當然沒有這麼簡單)~~

![設定 GitHub Pages](oqy27eg.webp)

## 部屬後問題處理方式

### (1) 找不到 resource 檔

```bash
Failed to load resource: the server responded with a status of 404 ()
```

![找不到 resource 檔](mVp6OJN.webp)

為什麼會產生此問題呢? 因為部屬上去 GitHub Pages 預設路徑為 [USERNAME].github.io/[REPOSITORY_NAME]，所以必須修改 index.html 裡面 base href。

- 方法一 ： 直接修改 index.html 的 base href

    ```HTML
        <!-- 修改成 Repository name -->
        <base href="/[REPOSITORY_NAME]/" />
    ```

    ![index.html](WsKE2fn.webp)

- 方法二 ： 改 YML 檔 **(建議)**

    ```YML
        # 修改 index.html 的 base href 設定
        - name: 修改 index.html 的 base href 設定
        run: sed -i 's/<base href="\/" \/>/<base href="\/[REPOSITORY_NAME]\/" \/>/g' release/wwwroot/index.html
    ```

### (2) 找不到 blazor.webassembly.js 檔

```bash
GET https://oneheed.github.io/BlazorGitHubPage/_framework/blazor.webassembly.js net::ERR_ABORTED 404
```

![ERR_ABORTED 404](bQzvDwL.webp)

產生此問題的原因，因為 GitHub Pages 部屬時，使用 Jekyll 來處理網站，它會自動忽略底線開頭的資料夾，所以造成網站會找不到檔案，故需要告訴它不要忽略檔案。

- 新增 `.nojekyll` 檔案

    ```YML
        # 新增 .nojekyll 檔
        - name: 新增 .nojekyll 檔
        run: touch release/wwwroot/.nojekyll
    ```

### 404 頁面

![404](LwA5FtZ.webp)

如果找不到頁面 GitHub Pages 會預設自己頁面，這個並非我們想要的，故可以將 index.html 複製成 404.html，如果找不到就會顯示 "Sorry, there's nothing at this address."。

- 複製 index.html 成 404.html

    ```YML
        # 複製 index.html 成 404.html
        - name: 複製 index.html 成 404.html
        run: cp release/wwwroot/index.html release/wwwroot/404.html
    ```

## 總結

經歷以上的問題的處理，終於成功將網站部屬上去並可以正常操作，如果有遇到上述沒有提到問題歡迎一起討論

![成功畫面](RlfarR0.webp)

最後 YML 檔

```YML
name: .NET

on:
  push:
    branches: [ master ]

jobs:
  deploy-to-github-pages:

    runs-on: ubuntu-latest

    steps:
    - uses: actions/checkout@v3
    - name: Setup .NET
      uses: actions/setup-dotnet@v2
      with:
        dotnet-version: 6.0.x
        
    - name: Publish .NET Core Project
      run: dotnet publish BlazorGitHubPages.csproj -c Release -o release --nologo
      
    # 修改 index.html 的 base href 設定
    - name: 修改 index.html 的 base href 設定
      run: sed -i 's/<base href="\/" \/>/<base href="\/BlazorGitHubPage\/" \/>/g' release/wwwroot/index.html
      
    # 新增 .nojekyll 檔
    - name: 新增 .nojekyll 檔
      run: touch release/wwwroot/.nojekyll
      
    # 複製 index.html 成 404.html
    - name: 複製 index.html 成 404.html
      run: cp release/wwwroot/index.html release/wwwroot/404.html
      
    # Commit wwwroot to GitHub Pages   
    - name: Commit wwwroot to GitHub Pages
      uses: JamesIves/github-pages-deploy-action@v4.3.3
      with:
          branch: gh-pages
          folder: release/wwwroot
```

最後完成 GitHub 網站

[https://oneheed.github.io/BlazorGitHubPage](https://oneheed.github.io/BlazorGitHubPage)

## 參考資料

[手把手將 Blazor WebAssembly 部署到 GitHub Pages](https://blog.poychang.net/publish-blazor-webassembly-app-to-github-page/)

[Github Pages 裝載和部署 ASP.NET Core Blazor WebAssembly](https://dotblogs.azurewebsites.net/jakeuj/2021/04/09/BlazorWebAssemblyGithubPage)

[BlazorOnGitHubPages](https://github.com/SteveSandersonMS/BlazorOnGitHubPages)

[裝載和部署 ASP.NET Core Blazor WebAssembly](https://docs.microsoft.com/zh-tw/aspnet/core/blazor/host-and-deploy/webassembly?view=aspnetcore-3.1#github-pages-2)

[How to deploy ASP.NET Blazor WebAssembly to GitHub Pages](https://swimburger.net/blog/dotnet/how-to-deploy-aspnet-blazor-webassembly-to-github-pages)

[How to deploy Blazor WebAssembly as static site in GitLab Pages](https://stackoverflow.com/questions/70213662/how-to-deploy-blazor-webassembly-as-static-site-in-gitlab-pages)

[Continuous integration and Continuous deployment of Blazor application to GitLab page.](https://bipinpaul.com/posts/continuous-integration-and-continuous-deployment-of-blazor-application-to-gitlab-page)

[Deploy to GitHub Pages](https://github.com/marketplace/actions/deploy-to-github-pages)

<!-- https://docs.microsoft.com/en-us/shows/on-net/intro-to-docfx -->
